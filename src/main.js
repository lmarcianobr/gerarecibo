import Vue from 'vue'
import App from './App.vue'
import router from './router'

import Buefy from 'buefy'
import '@mdi/font/css/materialdesignicons.css'
import money from 'v-money'

Vue.use(Buefy)
Vue.use(money, {precision: 2})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
