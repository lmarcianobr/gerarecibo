import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'form',
      component: () => import('./views/Novo.vue')
    },
    {
      path: '/recibo/:id',
      name: 'recibo',
      component: () => import('./views/Recibo.vue')
    }
  ]
})
